from math import isnan
from numpy import array, zeros


class OutlierDetectorFrugralMAD(object):

    def __init__(self, k: float = 4.0):
        super().__init__()
        if k < 1 or k > 5:
            raise ValueError('Spread factor seems poorly chosen')        
        self.k = k
        self.alpha = .1
        self.med, self.mad = None, None

    def process(self, x: float) -> bool:
        if x is None or isnan(x):
            return None

        self.alpha = 0.1 if self.mad is None else self.mad/100       

        self.med = x if self.med is None else self.med + self.alpha if x > self.med else self.med - self.alpha
        self.mad = abs(x) if self.mad is None else self.mad + self.alpha if abs(x - self.med) > self.mad else self.mad - self.alpha

        low_fence = self.med - self.k*self.mad
        high_fence = self.med + self.k*self.mad

        if x >= high_fence or x <= low_fence:
            return True

        return False

    def process_with_details(self, x: float) -> array:
        if x is None or isnan(x):
            return None
        ret = zeros(3)

        self.alpha = 0.1 if self.mad is None else self.mad/100

        self.med = x if self.med is None else self.med + self.alpha if x > self.med else self.med - self.alpha
        self.mad = abs(x) if self.mad is None else self.mad + self.alpha if abs(x - self.med) > self.mad else self.mad - self.alpha
        
        low_fence = self.med - self.k*self.mad
        high_fence = self.med + self.k*self.mad
        ret[0] = low_fence
        ret[1] = high_fence

        if x >= high_fence or x <= low_fence:
            ret[2] = 1

        return ret    

class OutlierDetectorFrugralMAD2(object):

    def __init__(self, k: float = 4.0, init_med_est: float = 0.0, init_mad_est: float = 0.0):
        super().__init__()
        if k < 1 or k > 5:
            raise ValueError('Spread factor seems poorly chosen')        
        self.k = k
        self.alpha = init_mad_est/100 if init_mad_est > 0 else .01
        self.med, self.mad = init_med_est, init_mad_est

    def process(self, x: float) -> bool:
        if x is None or isnan(x):
            return None

        self.med += self.alpha if x > self.med else -self.alpha
        self.mad += self.alpha if abs(x - self.med) > self.mad else -self.alpha
        low_fence = self.med - self.k*self.mad
        high_fence = self.med + self.k*self.mad

        if x >= high_fence or x <= low_fence:
            return True

        return False

    def process_with_details(self, x: float) -> array:
        if x is None or isnan(x):
            return None
        ret = zeros(3)

        self.med += self.alpha if x > self.med else -self.alpha
        self.mad += self.alpha if abs(x - self.med) > self.mad else -self.alpha
        low_fence = self.med - self.k*self.mad
        high_fence = self.med + self.k*self.mad
        ret[0] = low_fence
        ret[1] = high_fence

        if x >= high_fence or x <= low_fence:
            ret[2] = 1

        return ret    
