from math import isnan
from collections import deque
from numpy import array, zeros, median, fabs


class OutlierDetectorMAD(object):
    def __init__(self, buffer_size: int = 11, k: float = 1.0, alpha: float = 0.0):
        super().__init__()
        if buffer_size < 1:
            raise ValueError("Buffer size should be larger than 1")
        if k < 1 or k > 20:
            raise ValueError("Spread factor seems ill chosen")
        self.buffer = deque(maxlen=buffer_size)
        self.k = k
        self.alpha = alpha
        self.low_fence, self.high_fence = None, None
        self.initialized = False

    def process(self, x: float) -> bool:
        if x is None or isnan(x):
            return None
        self.buffer.append(x)

        if len(self.buffer) == self.buffer.maxlen:
            med = median(self.buffer)
            mad = median(fabs(self.buffer - med))
            low_fence = med - self.k * mad
            high_fence = med + self.k * mad
            self.low_fence = (
                low_fence
                if self.low_fence is None
                else self.alpha * self.low_fence + (1 - self.alpha) * low_fence
            )
            self.high_fence = (
                high_fence
                if self.high_fence is None
                else self.alpha * self.high_fence + (1 - self.alpha) * high_fence
            )

            if x >= self.high_fence or x <= self.low_fence:
                return True

        return False

    def process_with_details(self, x: float) -> array:
        if x is None or isnan(x):
            return None
        if len(self.buffer) == self.buffer.maxlen - 1:
            print("buffer filled")
            self.initialized = True
        self.buffer.append(x)

        ret = zeros(3)

        if self.initialized:
            med = median(self.buffer)
            mad = median(fabs(self.buffer - med))
            low_fence = med - self.k * mad
            high_fence = med + self.k * mad
            self.low_fence = (
                low_fence
                if self.low_fence is None
                else self.alpha * self.low_fence + (1 - self.alpha) * low_fence
            )
            self.high_fence = (
                high_fence
                if self.high_fence is None
                else self.alpha * self.high_fence + (1 - self.alpha) * high_fence
            )
            ret[0] = self.low_fence
            ret[1] = self.high_fence

            if x >= self.high_fence or x <= self.low_fence:
                ret[2] = 1

        return ret
