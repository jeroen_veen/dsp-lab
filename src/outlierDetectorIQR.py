from math import isnan
from collections import deque
from numpy import array, zeros
from statistics import quantiles


class OutlierDetectorIQR(object):
    def __init__(self, buffer_size: int = 11, k: float = 1.5):
        super().__init__()
        if buffer_size < 1:
            raise ValueError("Buffer size should be larger than 1")
        if k < 1 or k > 4:
            raise ValueError("Spread factor seems ill chosen")
        self.buffer = deque(maxlen=buffer_size)
        self.k = k
        self.initialized = False

    def process(self, x: float) -> bool:
        if x is None or isnan(x):
            return None
        if len(self.buffer) == self.buffer.maxlen - 1:
            print("buffer filled")
            self.initialized = True
        self.buffer.append(x)

        if self.initialized:
            quartiles = array(quantiles(self.buffer, n=4))
            iqr = quartiles[2] - quartiles[0]
            low_fence = quartiles[1] - self.k * iqr
            high_fence = quartiles[1] + self.k * iqr

            if x >= high_fence or x <= low_fence:
                return True

        return False

    def process_with_details(self, x: float) -> array:
        if x is None or isnan(x):
            return None
        if len(self.buffer) == self.buffer.maxlen - 1:
            print("buffer filled")
            self.initialized = True
        self.buffer.append(x)

        ret = zeros(3)

        if self.initialized:
            quartiles = quantiles(self.buffer, n=4)
            iqr = quartiles[2] - quartiles[0]
            low_fence = quartiles[1] - self.k * iqr
            high_fence = quartiles[1] + self.k * iqr
            ret[0] = low_fence
            ret[1] = high_fence

            if x >= high_fence or x <= low_fence:
                ret[2] = 1

        return ret
