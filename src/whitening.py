from math import isnan
from collections import deque
from numpy import array, zeros, median, fabs
from statistics import quantiles
import numpy as np
from scipy.linalg import toeplitz
from scipy.signal import filtfilt, lfilter, periodogram
# Make sure to check C++/CLI support and MVC++ latest ARM build tools when installing Build Tools for Visual Studio
from spectrum import Periodogram, parma, arma_estimate, arma2psd, pcovar


def noise_psd(N, psd = lambda f: 1):
        X_white = np.fft.rfft(np.random.randn(N));
        S = psd(np.fft.rfftfreq(N))
        # Normalize S
##        S = S / np.sqrt(np.mean(S**2))
        X_shaped = X_white * S
        return np.fft.irfft(X_shaped);

def PSDGenerator(f):
    return lambda N: noise_psd(N, f)

@PSDGenerator
def white_noise(f):
    return 1;

@PSDGenerator
def blue_noise(f):
    return np.sqrt(f);

@PSDGenerator
def violet_noise(f):
    return f;

@PSDGenerator
def brownian_noise(f):
    return 1/np.where(f == 0, float('inf'), f)

@PSDGenerator
def pink_noise(f):
    return 1/np.where(f == 0, float('inf'), np.sqrt(f))


def zca_whitening_matrix(X):
    """
    Function to compute ZCA whitening matrix (aka Mahalanobis whitening).
    INPUT:  X: [M x N] matrix.
        Rows: Variables
        Columns: Observations
    OUTPUT: ZCAMatrix: [M x M] matrix
    """
    # Covariance matrix [column-wise variables]: Sigma = (X-mu)' * (X-mu) / N
    sigma = np.cov(X, rowvar=True) # [M x M]
    # Singular Value Decomposition. X = U * np.diag(S) * V
    U,S,V = np.linalg.svd(sigma)
        # U: [M x M] eigenvectors of sigma.
        # S: [M x 1] eigenvalues of sigma.
        # V: [M x M] transpose of U
    # Whitening constant: prevents division by zero
    epsilon = 1e-5
    # ZCA Whitening matrix: U * Lambda * U'
    ZCAMatrix = np.dot(U, np.dot(np.diag(1.0/np.sqrt(S + epsilon)), U.T)) # [M x M]
    return ZCAMatrix


def generate_power_law_noise(N: int, alpha: float) -> np.array:
        M = 10
        H_a = [1]
        for k in range(1, M):
                a = (k - 1 - alpha/2) * H_a[k-1]/k
                H_a.extend([a])
        print(H_a)
        noise = lfilter([1], H_a, np.random.randn(N))

        return noise, H_a


##if __name__ == "__main__":
from numpy import random, vectorize, where
import matplotlib.pyplot as plt

NFFT = 8192
N = 50000
alpha = 2

n1 = brownian_noise(N) # pink_noise(N) #
n2, H_a = generate_power_law_noise(N, alpha)
n1 /= np.std(n1 - np.mean(n1))
n2 -= np.mean(n2)
n2 /= np.std(n2)

P_n1 = Periodogram(n1).psd/np.sqrt(N)
P_n2 = Periodogram(n2).psd/np.sqrt(N)

##ar, ma, rho = arma_estimate(n2, 10, 1, 30)
##print(ar, ma, rho)
##P_n_est = arma2psd(ar, ma, rho=rho, NFFT=4096)
##p = pcovar(n2, 5, NFFT=4096)

# Full data covariance matrix estimate
x, order, NFFT = n2, 2, 4096
from scipy import linalg

# Batch based covariance estimate
#  Since we deal with pole estimation, datamatrix should be large
# Make a plot of estimation accuracy vs data matrix size
B, N = 512, 512 # 20*order, 20*order  # 32, 32  # 128, 128 #256, 512 # 512, 1024
X = np.zeros((N-order, order+1))
eta = .1
for i in range(1, len(x - N)//B - N//B):
        ind = (i-1)*B
        x_ = x[ind:ind+N]
        X_ = linalg.toeplitz(x_[order:], x_[order::-1]) #[order:], x_[order::-1])
        X = eta*X + (1-eta)*X_

Xc = np.array(X[:, 1:])
X1 = np.array(X[:, 0])
ar, _residues, _rank, _singular_values = linalg.lstsq(-Xc, X1)
print(ar)

# Next step: make block adaptive algorithm, compute AIC and adapt order, save intermediate AR coefficients, and order
#  Can we compute matrix inversion using approximation of Toeplitz? Diagonalize with DFT or something?

p = arma2psd(A=ar, T=1, NFFT=NFFT)


fig0, ax0 = plt.subplots()
ax0.plot(n1)
ax0.plot(n2, 'k')

f_min, f_max = 1e-4, 0.5
fig1, ax1 = plt.subplots()
##ax1.semilogx(np.arange(0, 0.5, 0.5/len(P_n1)), 10*np.log10(P_n1))
ax1.semilogx(np.arange(0, 0.5, 0.5/len(P_n2)), 10*np.log10(P_n2), 'k')
##ax1.semilogx(np.arange(0, 0.5, 0.5/len(P_n_est)), 10*np.log10(P_n_est), 'c')
##ax1.semilogx(np.arange(0, 0.5, 0.5/len(p)), 10*np.log10(p), 'm')
ax1.plot([f_min, f_max], [0, 10*np.log10(pow(f_max, -alpha)) - 10*np.log10(pow(f_min, -alpha))], 'r--')

ax1.grid(True)
ax1.set_xlim([f_min, f_max])
##ax1.set_ylim([1e-5, 1e3])
ax1.legend(['generated', 'theoretical']) #['psd generator', 'Kasdin', 'arma_estimate', 'pcovar', 'theoretical'])

ax1.set_xlabel('normalized frequency')
ax1.set_ylabel('normalized power')
ax1.set_title('Brownian noise')

filtered = filtfilt([1, -1], 1, n2)
filtered = filtered/np.std(filtered)

fig3, ax3 = plt.subplots()
ax3.plot(filtered)
ax3.plot(n2, 'k')
ax3.legend(['whitened', 'colored'])
ax3.set_xlabel('sample [#]')
ax3.set_ylabel('value')
ax3.set_title('Brownian noise whitening')


##
##y1 = lfilter(H_a, [1], n1)
##y2 = lfilter(H_a, [1], n2)
#### TODO: denominator should equal the integral of the filter response, how to compute?
##
##fig3, ax3 = plt.subplots()
##ax3.plot(y1)
##ax3.plot(y2, 'k')
##
##f, P_y1 = periodogram(y1)
##f, P_y2 = periodogram(y2)
##f_min, f_max = 1e-4, 0.5
##
##fig4, ax4 = plt.subplots()
##ax4.loglog(f, P_y1)
##ax4.loglog(f, P_y2, 'k')
##ax4.grid(True)
##ax4.set_xlim([f_min, f_max])
##ax4.set_xlabel('normalized frequency')
##ax4.set_ylabel('normalized power')
##ax4.set_title('Whitened noise by inverting')
##
##
##
##

## TODO: estimate autocovariance matrix, compute AR coefficientsand invert

##
####    y = vectorize(od.process)(x)
####    y = vectorize(od.process_with_details, signature='()->(n)')(x)
####    ind_flags = where(y[:,2]>0)[0]
##
##B = 512
##N = 1024
##acf = 0
##for i in range(0, len(x - N)//B):
##    ind = (i-1)*B
##    if ind >= 0:
##        x_ = x[ind:ind+N]
####        https://www.gaussianwaves.com/2015/05/auto-correlation-matrix-in-matlab/
##        r = np.correlate(x_,x_,mode='full')
##        acf += np.convolve(x_,np.conj(x_)[::-1]) # using Method 2 to compute Auto-correlation sequence
##        Rxx=acf[2:]; # R_xx(0) is the center element
##        Rx = toeplitz(Rxx,np.hstack((Rxx[0], np.conj(Rxx[1:]))))
####        ZCAMatrix = zca_whitening_matrix(x_) # get ZCAMatrix
##acf /= i
####
##fig0, ax0 = plt.subplots()
##ax0.plot(acf)
####    ##ax0.grid(True)
####    ax0.set_title("Sliding window outlier detector (MAD)")
####    ax0.vlines(ind_flags, -4, 4, color='xkcd:cerise', linestyle=':')
####    ax0.plot(y[:,0], color='xkcd:light blue', label='low fence')
####    ax0.plot([0,len(x)], 2*[-3,], color='xkcd:sky blue', linestyle='--', label='theoretical low fence')
####    ax0.plot(y[:,1], color='xkcd:light blue', label='high fence')
####    ax0.plot([0,len(x)], 2*[3,], color='xkcd:sky blue', linestyle='--', label='theoretical high fence')
####    ax0.plot(x, color='xkcd:navy', linewidth=2, label='input')
####    ax0.set_xlabel('sample [#]')
####    ##    ax0.legend(loc='best')
####
plt.show(block=True)
