from distutils.ccompiler import new_compiler
from os import remove

cpp_code = """
#include <cmath>
#include <stdexcept>
#include <vector>

extern "C" {
    void your_cpp_function(double* x, int size) {
        // Your C++ implementation here
        for (int i = 0; i < size; ++i) {
            // Modify x in-place or perform other operations
            x[i] = std::sin(x[i]);
        }
    }
}
"""

# Save the C++ code to a file (optional)
with open('your_cpp_code.cpp', 'w') as cpp_file:
    cpp_file.write(cpp_code)

compiler = new_compiler()

compile_args = ['-std=c++11']  # Add any necessary compilation flags
compiler.shared_lib_extension = ".so"  # Set the extension based on your platform
compiler.output_dir = '.'  # Set the output directory

# Compile and link the C++ code
obj_file, = compiler.compile(['your_cpp_code.cpp'], output_dir='.')
# compiler.link_shared_lib([obj_file], 'your_cpp_library', output_dir='.')

# Load the compiled C++ library
# your_cpp_library = ctypes.CDLL('./your_cpp_library.so')

# Define the input data
# x = [1.0, 2.0, 3.0]
# x_array = (ctypes.c_double * len(x))(*x)

# # Call the C++ function
# your_cpp_library.your_cpp_function(x_array, len(x))

# # Print the modified data
# print(list(x_array))



from numpy import array, vectorize
from scipy import signal
import sys

sos = signal.butter(N=5, Wn=250, btype='lowpass', output='sos', fs=1600)    
biquad = Biquads(sos)

x = array([-1.0]*50 + [1.0]*50 + [0.0]*50)

f1 = signal.sosfilt(sos, x)
f2 = vectorize(biquad.process)(x)

fig0, ax0 = plt.subplots()
ax0.grid(True)
ax0.set_title("Filter response")
ax0.plot(x, 'k--', label='input')
ax0.plot(f1, 'b', alpha=0.5, linewidth=2, label='sosfilt')
ax0.plot(f2, 'r', alpha=0.25, linewidth=4, label='biquad')
ax0.set_xlabel('sample [#]')
ax0.legend(loc='best')
