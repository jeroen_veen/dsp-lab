import numpy as np
from scipy.signal import lfilter
import matplotlib.pyplot as plt
from spectrum import Periodogram, parma, arma_estimate, arma2psd, pcovar
from power_law_noise import pink_noise, brownian_noise, pacf, generate_from_AR_model, generate_from_psd


NFFT = 1024 # 8192
max_lag = 10
alpha_step = .01
alpha_max = 2.0

##alpha_range = np.linspace(0, alpha_max, 4) #
##alpha_range = [.5,1,2]
alpha_range = np.arange(alpha_step, alpha_max, alpha_step)
##alpha_range = [0, 1, 2]
r = np.zeros((max_lag + 1, len(alpha_range)))
P = np.zeros(((NFFT >> 1) + 1, len(alpha_range)))
for i, alpha in enumerate(alpha_range):
    n, h = generate_from_AR_model(NFFT, alpha)
    r[:,i] = pacf(n, max_lag)
    P_n = Periodogram(n).psd
    P[:,i] = P_n/P_n[0]
    

fig0, ax0 = plt.subplots()
ax0.semilogx(np.arange(0, 0.5, 0.5/len(P)), 10*np.log10(P))
ax0.legend([f'alpha = {alpha}' for alpha in alpha_range])

k = np.arange(0, max_lag) + 1
##model = np.sqrt(1 / k**(2-alpha_range[1]))
##model = 1 / k**(2-alpha_range[0]/2)
##model = 1 / k**(2 - .5 * alpha_range[1]**2)
beta = 1 - alpha_range[1]/2
model = k**-beta

n, h = generate_from_AR_model(NFFT, 0)
r_white = pacf(n, max_lag)
n, h = generate_from_AR_model(NFFT, 1)
r_pink = pacf(n, max_lag)
n, h = generate_from_AR_model(NFFT, 2)
r_brown = pacf(n, max_lag)

fig1a, ax1a = plt.subplots()
ax1a.plot(r_white, 'b', linewidth=2)
ax1a.plot(r_pink, 'pink', linewidth=2)
ax1a.plot(r_brown, 'brown', linewidth=2)
ax1a.grid(True)
ax1a.plot(model, 'k--')
ax1a.set_xlabel('lag')
ax1a.set_ylabel('amplitude')
ax1a.legend([f'alpha = {alpha}' for alpha in alpha_range], loc='upper right')
ax1a.set_xlim([0, max_lag-1])
ax1a.set_ylim([-.1, 1.1])
ax1a.set_title('autocorrelation function')


fig1, ax1 = plt.subplots()
ax1.plot(r)
ax1.grid(True)
ax1.plot(model, 'k--')
ax1.set_xlabel('lag')
ax1.set_ylabel('autocorrelation function')
ax1.legend([f'alpha = {alpha}' for alpha in alpha_range], loc='upper right')
ax1.set_xlim([0, max_lag-1])
ax1.set_ylim([-.1, 1.1])


r = [r_[1]/r_[0] for r_ in r.transpose()]
##model = [1 - 2**(-2*alpha) for alpha in alpha_range]
##model = [1 - np.exp(-alpha) for alpha in alpha_range]
##model = [.5 + .5*np.tanh(1.75*(alpha-.75)) for alpha in alpha_range]  # tanh
model = [1/(1 + np.exp(-3.5*(alpha-.75))) for alpha in alpha_range]  # sigmoid
model = [1/(1 + np.exp(-4 * (alpha-.75))) for alpha in alpha_range]  # sigmoid

fig2, ax2 = plt.subplots()
ax2.plot(alpha_range, r) # 10*np.log10(r))
ax2.plot(alpha_range, model, 'k')
ax2.grid(True)
##ax2.legend([f'alpha = {alpha}' for alpha in alpha_range])
ax2.set_xlabel('alpha')
ax2.set_ylabel('lag 1 coefficient')
ax2.set_title('Autocorrelation lag 1 of power law processes')

plt.show(block=False)

