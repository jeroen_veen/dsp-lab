from math import isnan
from numpy import array


class Biquad(object):

    def __init__(self, sos: array):
        super().__init__()
        if len(sos) != 6:
            raise ValueError('Invalid number of coefficients')
        self.dc_gain = sos[:3].sum() / sos[3:].sum()
        self.b_0, self.b_1, self.b_2, self.a_0, self.a_1, self.a_2 = sos
        if self.a_0 != 1:
            raise ValueError('First feedback coefficient (a_0) is assumed to be equal to 1')
        self.s = 2*[0]
        self.initialized = False    

    def process(self, x: float) -> float:
        if x is None or isnan(x):
            return None
        if self.initialized:
            y = self.b_0 * x + self.s[0]
            self.s[0] = self.b_1 * x - self.a_1 * y + self.s[1]
            self.s[1] = self.b_2 * x - self.a_2 * y            
        else:
            y = x * self.dc_gain
            self.s[1] = self.b_2 * x - self.a_2 * y
            self.s[0] = self.b_1 * x - self.a_1 * y + self.s[1]
            self.initialized = True
        return y


class Biquads(object):

    def __init__(self, sos: array):
        super().__init__()
        self.N = sos.shape[0]
        self.cascade = [Biquad(sos_) for sos_ in sos]

    def process(self, x: float) -> float:
        y = x
        for i in range(self.N):
            y = self.cascade[i].process(y)
        return y        


if __name__ == "__main__":
    from numpy import vectorize
    from scipy import signal
    import matplotlib.pyplot as plt

    sos = signal.butter(N=5, Wn=250, btype='lowpass', output='sos', fs=1600)    
    biquad = Biquads(sos)

    x = array( [-1.0]*50 + [1.0]*50 + [0.0]*50)

    f1 = signal.sosfilt(sos, x)
    f2 = vectorize(biquad.process)(x)

    fig0, ax0 = plt.subplots()
    ax0.grid(True)
    ax0.set_title("Filter response")
    ax0.plot(x, 'k--', label='input')
    ax0.plot(f1, 'b', alpha=0.5, linewidth=2, label='sosfilt')
    ax0.plot(f2, 'r', alpha=0.25, linewidth=4, label='biquad')
    ax0.set_xlabel('sample [#]')
    ax0.legend(loc='best')
    

    plt.show()


