from numpy import zeros_like

def estimate_lag_1_correlation(x, eta):
    k = 0.5 # 1.4826
    med, mad, r_ = None, None, None
    r = zeros_like(x)
    for i, x_ in enumerate(x):
        alpha = 0.1 if mad is None else mad/100
        med = x_ if med is None else med + alpha if x_ > med else med - alpha
        mad = abs(x_) if mad is None else mad + alpha if abs(x_ - med) > mad else mad - alpha
        x_ -= med
       
        if i > 0:
            if r_ is None:
                r_ = abs(x_ * x_prev_)
            else:
                sigma = k * mad
                r_ = eta * r_ + (1-eta) * x_ * x_prev_ / (sigma**2)
            r[i] = r_
        x_prev_ = x_
    
    return r