import numpy as np
from scipy.signal import filtfilt, lfilter, periodogram

# Make sure to check C++/CLI support and MVC++ latest ARM build tools when installing Build Tools for Visual Studio
from spectrum import Periodogram
from scipy import linalg


def noise_psd(N, psd=lambda f: 1):
    X_white = np.fft.rfft(np.random.randn(N))
    S = psd(np.fft.rfftfreq(N))
    S = S / np.sqrt(np.mean(S**2))
    X_shaped = X_white * S
    return np.fft.irfft(X_shaped)


def PSDGenerator(f):
    return lambda N: noise_psd(N, f)


@PSDGenerator
def white_noise(f):
    return 1


@PSDGenerator
def blue_noise(f):
    return np.sqrt(f)


@PSDGenerator
def violet_noise(f):
    return f


@PSDGenerator
def brownian_noise(f):
    return 1 / np.where(f == 0, float("inf"), f)


@PSDGenerator
def pink_noise(f):
    return 1 / np.where(f == 0, float("inf"), np.sqrt(f))


def generate_from_AR_model(nr_of_samples: int, alpha: float) -> np.array:
    k = 1
    h_a = [1]
    while 1:
        a = (k - 1 - alpha / 2) * h_a[k - 1] / k
        if abs(a) < 0.01:
            break
        k += 1
        h_a.extend([a])
#     print(h_a)
    noise = lfilter([1], h_a, np.random.randn(nr_of_samples))

    ##        order = 10
    ##        h_a = [1]
    ##        for k in range(1, order):
    ##                a = (k - 1 - alpha/2) * h_a[k-1]/k
    ##                h_a.extend([a])
    ##        print(h_a)
    ##        noise = lfilter([1], h_a, np.random.randn(nr_of_samples))

    return noise, h_a


def generate_from_psd(nr_of_samples: int, alpha: float) -> np.array:
    N = np.fft.rfft(np.random.randn(nr_of_samples))
    f = np.fft.rfftfreq(nr_of_samples)
    H = (f + 0.5 * f[1]) ** (-alpha / 2)
    H /= np.sqrt(np.mean(np.abs(H) ** 2))
    X = N * H
    return np.fft.irfft(X)


def estimate_covariance(
    x, order: int = 2, step_size: int = 512, block_size: int = 1024, eta: float = 0.1
):
    # Batch based covariance estimate
    #  Since we deal with pole estimation, datamatrix should be large
    # Make a plot of estimation accuracy vs data matrix size
    X = np.zeros((block_size - order, order + 1))
    for i in range(1, len(x - block_size) // step_size - block_size // step_size):
        ind = (i - 1) * step_size
        x_ = x[ind : ind + block_size]
        X_ = linalg.toeplitz(x_[order:], x_[order::-1])  # [order:], x_[order::-1])
        X = eta * X + (1 - eta) * X_

    Xc = np.array(X[:, 1:])
    X1 = np.array(X[:, 0])
    ar, _residues, _rank, _singular_values = linalg.lstsq(Xc, X1)

    return ar


def pacf(x: np.array, max_lag: int) -> np.array:
    r = np.correlate(x, x, mode="full")[-len(x) :]
    return r[: max_lag + 1] / r[0]


if __name__ == "__main__":
    import matplotlib.pyplot as plt

    N = 8192
    f_min, f_max = 1e-3, 0.5
    P_min, P_max = -60, 0

    n_brown_1 = generate_from_psd(N, alpha=2)  # brownian_noise(N)
    ##        n_brown_1 -= np.mean(n_brown_1)
    n_brown_1 /= np.std(n_brown_1 - np.mean(n_brown_1))
    n_pink_1 = generate_from_psd(N, alpha=1)  # pink_noise(N)
    ##        n_pink_1 -= np.mean(n_pink_1)
    n_pink_1 /= np.std(n_pink_1 - np.mean(n_pink_1))
    n_brown_2, h_a_1 = generate_from_AR_model(N, alpha=2)
    p_1 = len(h_a_1)
    ##        n_brown_2 -= np.mean(n_brown_2)
    n_brown_2 /= np.std(n_brown_2 - np.mean(n_brown_2))
    n_pink_2, h_a_2 = generate_from_AR_model(N, alpha=1)
    p_2 = len(h_a_2)
    ##        n_pink_2 -= np.mean(n_pink_2)
    n_pink_2 /= np.std(n_pink_2 - np.mean(n_pink_2))
    ar_brown_1 = pacf(n_brown_1, 10)
    ar_brown_2 = pacf(n_brown_2, 10)
    ar_pink_1 = pacf(n_pink_1, 10)
    ar_pink_2 = pacf(n_pink_2, 10)

    P_n_brown_1 = 10 * np.log10(Periodogram(n_brown_1).psd / np.sqrt(N))
    P_n_pink_1 = 10 * np.log10(Periodogram(n_pink_1).psd / np.sqrt(N))
    P_n_brown_2 = 10 * np.log10(Periodogram(n_brown_2).psd / np.sqrt(N))
    P_n_pink_2 = 10 * np.log10(Periodogram(n_pink_2).psd / np.sqrt(N))

    f = (np.arange(0, 0.5, 0.5 / len(P_n_brown_1)),)

    fig0, ax0 = plt.subplots()
    ax0.semilogx(np.arange(0, 0.5, 0.5 / len(P_n_pink_1)), P_n_pink_1, "b", alpha=0.5)
    ax0.semilogx(np.arange(0, 0.5, 0.5 / len(P_n_pink_2)), P_n_pink_2, "c", alpha=0.5)
    alpha = 1
    ax0.plot(
        [f_min, f_max],
        [0, 10 * np.log10(pow(f_max, -alpha)) - 10 * np.log10(pow(f_min, -alpha))],
        "r--",
    )
    ax0.grid(True)
    ax0.set_xlim([f_min, f_max])
    ax0.set_ylim([P_min, P_max])
    ax0.set_xlabel("normalized frequency")
    ax0.set_ylabel("normalized power [dB]")
    ax0.set_title("Pink noise")
    ax0.legend(["PSD", "AR(" + str(p_1) + ")", "theoretical"])

    fig1, ax1 = plt.subplots()
    ax1.semilogx(np.arange(0, 0.5, 0.5 / len(P_n_brown_1)), P_n_brown_1, "b", alpha=0.5)
    ax1.semilogx(np.arange(0, 0.5, 0.5 / len(P_n_brown_2)), P_n_brown_2, "c", alpha=0.5)
    alpha = 2
    ax1.plot(
        [f_min, f_max],
        [0, 10 * np.log10(pow(f_max, -alpha)) - 10 * np.log10(pow(f_min, -alpha))],
        "r--",
    )
    ax1.grid(True)
    ax1.set_xlim([f_min, f_max])
    ax1.set_ylim([P_min, P_max])
    ax1.set_xlabel("normalized frequency")
    ax1.set_ylabel("normalized power [dB]")
    ax1.set_title("Brownian noise")
    ax1.legend(["PSD", "AR(" + str(p_2) + ")", "theoretical"])

    ##        fig2, ax2 = plt.subplots()
    ##        ax2.plot(ar_pink_1, 'b', alpha=.5)
    ##        ax2.plot(ar_pink_2, 'c', alpha=.5)
    ##        alpha = 1
    ##        beta = 1 - alpha/2
    ##        ax2.plot(np.arange(0, len(ar_brown_1)-1, 1), np.arange(1, len(ar_brown_1), 1)**-beta, 'r--')
    ##        ax2.set_xlabel('lag')
    ##        ax2.set_ylabel('normalized coefficient')
    ##        ax2.set_title('Pink noise, S(f) = 1/f')
    ##
    ##        fig3, ax3 = plt.subplots()
    ##        ax3.plot(ar_brown_1, 'b', alpha=.5)
    ##        ax3.plot(ar_brown_2, 'c', alpha=.5)
    ##        alpha = 2
    ##        beta = 1 - alpha/2
    ##        ax3.plot(np.arange(0, len(ar_brown_1)-1, 1), np.arange(1, len(ar_brown_1), 1)**-beta, 'r--')
    ##        ax3.set_xlabel('lag')
    ##        ax3.set_ylabel('normalized coefficient')
    ##        ax3.set_title('Brown noise, S(f) = 1/f^2')

    plt.show(block=True)
