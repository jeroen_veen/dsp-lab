StatusCode        : 200
StatusDescription : OK
Content           : # Aptos illa Peripha
                    
                    ## Per quarto inque est timorem nymphae
                    
                    Lorem markdownum illam ore *colebat* perque cava undis meruisse si, sic ferae
                    vehebat obviaque: nostra convellere. Vertor statuit, fauces...
RawContent        : HTTP/1.1 200 OK
                    Transfer-Encoding: chunked
                    Connection: keep-alive
                    access-control-allow-origin: *
                    Content-Type: text/plain; charset=utf-8
                    Date: Mon, 25 Sep 2023 15:05:02 GMT
                    Server: nginx/1.14.2...
Forms             : {}
Headers           : {[Transfer-Encoding, chunked], [Connection, keep-alive], [access-control-allow-origin, *], [Content-Type, 
                    text/plain; charset=utf-8]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 2673
