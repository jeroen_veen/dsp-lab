```python
# mkdocs: render
# mkdocs: hidecode

import sys
sys.path.append('src/')  
```

# Welcome to digital signal processing lab!

Nowadays, digital signal processing plays a pivotal role in our daily lives and contributes to significant scientific advancements. For instance, a smartphone serves as a hub for signal processing, performing various functions that facilitate communication, message exchange, storage of music, pictures, videos, and more. It is relevant to both streaming data and static (stored) data. Digital signal processing finds applications in various fields like audio and speech processing, sonar, radar, and sensor array processing, along with tasks such as estimating spectral density, statistical signal processing, digital image processing, telecommunications signal processing, control systems, biomedical engineering, and seismology. In these fields, DSP offers numerous advantages over analog processing in applications like error detection and correction during transmission, as well as data compression. 

<!-- DSP is utilized for diverse objectives, including improving visual images, recognizing and generating speech, and compressing data for storage and transmission. -->

Let's delve into a specific application in more detail: Telecommunications, which encompasses the transmission of signs, signals, messages, words, writings, images, sounds, or information of any kind through wire, radio, optical, or electromagnetic systems. Telecommunication takes place when the exchange of information involves the use of technology. This information is conveyed either electrically through physical media like cables or via electromagnetic radiation. Communication paths are often segmented into channels, allowing the advantages of multiplexing. The term "telecommunications" is frequently used in its plural form due to its association with various technologies, considering communicatio, the Latin term denoting the social process of information exchange.

Historical methods of long-distance communication involved visual signals like beacons, smoke signals, semaphore telegraphs, signal flags, and optical heliographs. Audio messages, such as coded drumbeats, lung-blown horns, and loud whistles, were also employed. In the 20th and 21st centuries, long-distance communication predominantly relies on electrical and electromagnetic technologies such as telegraph, telephone, teleprinter networks, radio, microwave transmission, fiber optics, and communication satellites.

Communication signals can be transmitted through analog or digital signals, each having its own characteristics. Analog signals vary continuously with respect to information, while digital signals encode information as discrete values (e.g., a set of ones and zeros). During propagation and reception, analog signals are susceptible to degradation by physical noise, whereas digital signals, thanks to their discrete nature, resist noise up to a certain threshold. This resistance to noise is a significant advantage of digital signals over analog signals.

In the contemporary era, the prevalence of mobile phones surpasses the global population. Telecommunications systems that incorporate multiple transmitters and receivers designed to cooperate and share the same physical channel are known as multiplex systems. Multiplexing significantly reduces costs by sharing physical channels. Telecommunication networks are designed with multiplexed systems, and the signals are switched at nodes to reach the correct destination terminal receiver.

Unlike the mechanical switches and amplifiers required for analog voice signals until the 1960s, Digital Signal Processing (DSP) converts audio signals into serial digital data streams. DSP allows easy intertwining and separation of bits, enabling multiple telephone conversations on a single channel. Beyond multiplexing and enhancing the robustness of phone calls, DSP is applied for data compression and audio enhancement processes, such as echo cancellation. 





For full documentation visit [mkdocs.org](https://www.mkdocs.org).

$$
E(\mathbf{v}, \mathbf{h}) = -\sum_{i,j}w_{ij}v_i h_j - \sum_i b_i v_i - \sum_j c_j h_j
$$


```Python
# mkdocs: render
# mkdocs: hidecode

import matplotlib.pyplot as plt
import numpy as np

xpoints = np.array([1, 8])
ypoints = np.array([3, 10])

plt.plot(xpoints, ypoints)
```

See https://kroki.io/examples.html

```kroki-blockdiag no-transparency=false
blockdiag {
  default_fontsize = 20;  // default value is 11
  
  adder [shape = circle, label="+", fontsize = 40];
  I [icon = "http://blockdiag.com/en/_static/internet-mail.png"];
  F [label = "\\(a_0 = 1 \\)", background = "_static/python-logo.gif"];

  blockdiag -> generates -> "block-diagrams";
  blockdiag -> is -> "very easy!" -> adder -> I -> F;

}
```

```kroki-blockdiag no-transparency=false
blockdiag {
  blockdiag -> generates -> "block-diagrams";
  blockdiag -> is -> "very easy!";

  blockdiag [color = "greenyellow"];
  "block-diagrams" [color = "pink"];
  "very easy!" [color = "orange"];
}
```

```kroki-seqdiag no-transparency=false
seqdiag {
  browser  -> webserver [label = "GET /index.html"];
  browser <-- webserver;
  browser  -> webserver [label = "POST /blog/comment"];
  webserver  -> database [label = "INSERT comment"];
  webserver <-- database;
  browser <-- webserver;
}
```

```kroki-excalidraw
@from_file:kroki-example.excalidraw
```

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
