# Power law noise whitening

Power-law noise, characterized by a distribution that decreases with amplitude following a power-law decay, often exhibits long-term correlations that extend infinitely. Dealing with such correlated noise is inherently challenging, making traditional signal processing methods less effective. As processing uncorrelated noise samples is typically more straightforward, one approach to deal with colored noise is to apply __whitening__ prior to further processing. Such a whitening transformation is a linear transformation that changes the input samples into a white noise sequence. In this exploration, we delve into a practical approach to power-law noise whitening, unraveling the intricacies of long-term correlations to unlock clearer and more actionable insights in signal processing.
 
## Power spectral density

White noise is a random process with constant power spectral density ([psd](https://en.wikipedia.org/wiki/Spectral_density)), i.e. having equal intensity at different frequencies. The term white noise is derived from white light, which contains all colors in the visible band. Similarly, in digital signal processing (DSP), white noise refers to a time-series of uncorrelated samples, typically zero-mean, with a flat power spectrum. Many DSP methods assume to deal with white noise processes, more specifically with additive white Gaussian noise ([AWGN](https://en.wikipedia.org/wiki/Additive_white_Gaussian_noise)). For AWGN, the frequency distribution of power is uniform, and the probability distribution of values is governed by a normal probability density function ([pdf](https://en.wikipedia.org/wiki/Probability_density_function)). 

In practical situations, we frequently come across correlated noise, wherein the current values are impacted by their preceding values, indicating a dependence on the process history. Such noise processes are commonly known as colored noise processes because they display non-uniform power spectra. A typical instance is __power law noise__, or $ 1 / f^\alpha $ noise, which is commonly observed in natural phenomena. The canonical case with $ \alpha = 1 $ is called __pink noise__ or __$ 1/f $ noise__, which has been discovered in the statistical fluctuations of an extraordinarily diverse number of physical and biological systems. Another common process, with $ \alpha = 2 $, is called __Brownian noise__, also known as Brown noise or random walk noise, which is the type of noise produced by Brownian motion. Visualizations of these common noise processes in both the time and frequency domain are shown below.


```python
# mkdocs: render
# mkdocs: hidecode

import matplotlib.pyplot as plt
from numpy import random, mean, std, log10, sqrt, arange
from spectrum import Periodogram

N = 512
f_min, f_max = 1e-3, 0.5
P_min, P_max = -60, 0

n_white = random.randn(N)

P_n_white = 10*log10(Periodogram(n_white).psd/sqrt(N))
f = arange(0, 0.5, 0.5/len(P_n_white))

fig, ax = plt.subplots(1,2)
ax[0].plot(n_white)
ax[0].grid(True)
ax[0].set_xlim([0, N])
ax[0].set_xlabel('time')
ax[0].set_ylabel('normalized amplitude')
ax[0].set_title('White noise')
ax[0].set_xticks([])
ax[0].set_yticks([])

ax[1].semilogx(f, P_n_white)
ax[1].semilogx([f_min, f_max], 2*[10*log10(1/sqrt(N)),], 'grey', linestyle=':') # linewidth=5)
ax[1].grid(True)
ax[1].set_xlim([f_min, f_max])
ax[1].set_xlabel('normalized frequency')
ax[1].set_ylabel('normalized power')
ax[1].set_title('White noise')
ax[1].set_xticks([])
ax[1].set_yticks([])
```

```python
# mkdocs: render
# mkdocs: hidecode

import matplotlib.pyplot as plt
from numpy import log10, mean, std, sqrt, arange
from power_law_noise import pink_noise, generate_from_psd, generate_from_AR_model, pacf
from spectrum import Periodogram

N = 512
f_min, f_max = 1e-3, 0.5
P_min, P_max = -60, 0
alpha = 1

n_pink_1 = generate_from_psd(N, alpha)
##        n_pink_1 -= mean(n_pink_1)
# n_pink_1 /= std(n_pink_1 - mean(n_pink_1))
# n_pink_2, H_a_2 = generate_from_AR_model(N, alpha=1)
##        n_pink_2 -= mean(n_pink_2)
# n_pink_2 /= std(n_pink_2 - mean(n_pink_2))
# ar_pink_1 = pacf(n_pink_1, 10)
# ar_pink_2 = pacf(n_pink_2, 10)                                

P_n_pink_1 = 10*log10(Periodogram(n_pink_1).psd/sqrt(N))
# P_n_pink_2 = 10*log10(Periodogram(n_pink_2).psd/sqrt(N))

f = arange(0, 0.5, 0.5/len(P_n_pink_1))

fig, ax = plt.subplots(1,2)
ax[0].plot(pink_noise(N), 'pink')
ax[0].grid(True)
ax[0].set_xlim([0, N])
ax[0].set_xlabel('time')
ax[0].set_ylabel('normalized amplitude')
ax[0].set_title('Pink noise')
ax[0].set_xticks([])
ax[0].set_yticks([])

ax[1].semilogx(f, P_n_pink_1, 'pink')
ax[1].semilogx([f_min, f_max], [0, 10*log10(pow(f_max, -alpha)) - 10*log10(pow(f_min, -alpha))], 'grey', linestyle=':')
ax[1].grid(True)
ax[1].set_xlim([f_min, f_max])
ax[1].set_ylim([P_min, P_max])
ax[1].set_xlabel('normalized frequency')
ax[1].set_ylabel('normalized power')
ax[1].set_title('Pink noise')
ax[1].set_xticks([])
ax[1].set_yticks([])
```

```python
# mkdocs: render
# mkdocs: hidecode

import matplotlib.pyplot as plt
from numpy import log10, mean, std, sqrt, arange
from power_law_noise import brownian_noise, pacf, generate_from_AR_model, generate_from_psd
from spectrum import Periodogram

N = 512
f_min, f_max = 1e-3, 0.5
P_min, P_max = -60, 0
alpha = 2

n_brown_1 = generate_from_psd(N, alpha)
##        n_brown_1 -= mean(n_brown_1)
# n_brown_1 /= std(n_brown_1 - mean(n_brown_1))
##ar_brown_1 = pacf(n_brown_1, 10)
##ar_brown_2 = pacf(n_brown_2, 10)
##
P_n_brown_1 = 10*log10(Periodogram(n_brown_1).psd/sqrt(N))
##P_n_brown_2 = 10*log10(Periodogram(n_brown_2).psd/sqrt(N))
##
f = arange(0, 0.5, 0.5/len(P_n_brown_1))

fig, ax = plt.subplots(1,2)

ax[0].plot(brownian_noise(N), 'brown')
ax[0].grid(True)
ax[0].set_xlim([0, N])
ax[0].set_xlabel('time')
ax[0].set_ylabel('normalized amplitude')
ax[0].set_title('Brownian noise')
ax[0].set_xticks([])
ax[0].set_yticks([])
ax[0].set_xticks([])
ax[0].set_yticks([])

ax[1].semilogx(f, P_n_brown_1, 'brown')
ax[1].semilogx([f_min, f_max], [0, 10*log10(pow(f_max, -alpha)) - 10*log10(pow(f_min, -alpha))], 'grey', linestyle=':')
ax[1].grid(True)
ax[1].set_xlim([f_min, f_max])
ax[1].set_ylim([P_min, P_max])
ax[1].set_xlabel('normalized frequency')
ax[1].set_ylabel('normalized power')
ax[1].set_title('Brownian noise')
ax[1].set_xticks([])
ax[1].set_yticks([])

```

## Probability density function

The probability density function (PDF) for a power-law distribution can be expressed as:
$$
x(\tau) = C \tau^{-\beta} \text{ for  } \tau > 0
$$
where $C $ is a normalization constant, and $ \beta $ is the exponent that characterizes the power-law decay of the distribution. Typically,
$$
C=\frac{\beta-1}{x_{min}^{1-\beta}}
$$
where $x_{min}$ is the smallest of the possible $x$ values.

Likewise, $ \alpha $ characterizes the power-law decay of the power spectral density. In the context of digital signal processing, specifically when addressing power-law behavior in signals or noise, the typical relationship between the exponents $ \alpha $ and $ \beta $ is often expressed as:
$$
\beta = \alpha + 1 
$$
Power-law noise, characterized by a power-law distribution, often does not have finite moments, including the variance. Specifically, when $\beta < 2 $, the variance is infinite, and higher-order moments may also be undefined.


## Power law noise generation

To date, no simple stochastic differential equations generating a power law process have been found, and even no generally recognized physical explanation of e.g. $ 1/f $ noise has been proposed [@ward_1f_2007]. Power law noise processes seem to exhibit infinitely 'long memory' or long range dependence and are hard to model in the vincinity of $ f=0 $, since a mean value cannot be computed. 
For that reason, we turn to approximations to model and generate power law noise.

### PSD approximation

In a first approximation, we shape the power spectral density function of a white noise process as to produce a power law noise process by frequency domain filtering. Consider the Fourier transform of a power law noise process $ x(t) $, given by
$$
X(f) = A f^{-\alpha/2} N(f) \text{ for  } f > 0
$$
where $ A $ is a normlization constant, and $ N(f) = \sqrt{N_0} $ is the Fourier transform of a white noise process $ n(t) $. Now, the power spectral density of $ x(t) $ equals
$$
S_{xx} (f) = \vert X(f) \vert^2 = A^2 f^{-\alpha} \vert N(f) \vert^2 = A^2 f^{-\alpha} N_0 \\ . 
$$

Using the fundamental Python package [NumPy](https://numpy.org/), we can easily generate a power law noise process according to the power spectral density (PSD) method.

```python
import numpy as np

N = np.fft.rfft(np.random.randn(nr_of_samples))
f = np.fft.rfftfreq(nr_of_samples)
H = (f + .5*f[1])**(-alpha/2)
H /= np.sqrt(np.mean(np.abs(H) ** 2))
X = N * H
x = np.fft.irfft(X)
```
Note that the power density within each discrete Fourier transform ([DFT](https://en.wikipedia.org/wiki/Discrete_Fourier_transform)) bin is determined by the centre frequency of that bin. Obviously, [pseudorandom](https://en.wikipedia.org/wiki/Pseudorandomness) noise is being used, since true randomness is hard to realize by a computer.

### All-pole model

In a second approximation, let's restate a parametric $ 1 / f^\alpha $ noise model from literature [@kasdin_discrete_1995]. This model corresponds to the classical result that the power spectrum of a uniform mixture of exponentially decaying autocorrelation functions has a 1/f form [@ward_1f_2007]. An __autoregressive model__ of order $ p $ of a correlated random sequence $ x[k] $ is given by
$$
\sum_{i=0}^p \theta_i x[k-i] = n[k] \\,
$$
where $ n[k] $ is a zero-mean uncorrelated sequence, i.e. white noise. The autoregressive AR(p) model specifies that the output $ x[k] $ depends linearly on previous output samples and on the white noise input. Assuming $ \theta_0 = 1 $ and rewriting the equation yields
$$
x[k] = n[k] - \sum_{i=1}^p \theta_k x[k-i] \\ .
$$
The __transfer function__ of a system that filters white noise to produce colored noise, specified by an AR(p) representation, is now given by
$$
H(z) = \frac {1} {1 + \sum_{i=1}^p \theta_k z^{-i}} \\,
$$
which is an all-pole filter, so there are no frequencies where the response equals zero. Now, a power law, or $ 1 / f^\alpha $, noise process can be modelled by AR(p) representation with
$$
\theta_i = \left ( i - 1 - \frac{\alpha}{2} \right ) \frac{\theta_{i-1}}{i}  \text{ for } i > 0 \\, 
$$
where again $ \alpha = 1 $ for pink noise; and $ \alpha = 2 $ for Brownian noise. In the latter case, $\theta_0 = 1, \theta_1 = -1, \theta_k = 0$ elsewhere.

To filter a white noise sequence, we apply the signal processing module of the Python package [SciPy](https://scipy.org/), which is an open-source software for mathematics, science, and engineering available for the Python programming language, see [scipy.signal](https://docs.scipy.org/doc/scipy/reference/signal.html#filter-design).

```python
from scipy.signal import lfilter

min_magn = 0.01
h_a = [1]
k = 1
while 1:
   a = (k - 1 - alpha/2) * h_a[k-1]/k
   if abs(a) < min_magn:
      break
   k += 1
   h_a.extend([a])
x = lfilter([1], h_a, np.random.randn(nr_of_samples))
```
Note that the order of the approximation is determined by a threshold set to leave out poles with magnitudes below 0.01 in this case.

Estimated power spectral density functions of pink noise and Brownian noise pseudorandom process realizations, obtained by both methods are plotted below. The psd is normalized to the total power, and frequency is normalized to the sampling frequency.

```python
# mkdocs: render
# mkdocs: hidecode

import matplotlib.pyplot as plt
from numpy import log10, mean, std, sqrt, arange
from power_law_noise import brownian_noise, pacf, generate_from_AR_model, generate_from_psd
from spectrum import Periodogram

N = 8192
f_min, f_max = 1e-3, 0.5
P_min, P_max = -60, 0
alpha = 2

n_pink_1 = generate_from_psd(N, alpha=1)  # pink_noise(N)
n_pink_2, h_a_2 = generate_from_AR_model(N, alpha=1)
p = len(h_a_2)
# n_pink_2 /= std(n_pink_2)

P_n_pink_1 = 10*log10(Periodogram(n_pink_1).psd/sqrt(N))
P_n_pink_2 = 10*log10(Periodogram(n_pink_2).psd/sqrt(N))

f = arange(0, 0.5, 0.5/len(P_n_pink_1)),

fig0, ax0 = plt.subplots()
ax0.semilogx(arange(0, 0.5, 0.5/len(P_n_pink_1)), P_n_pink_1, 'b', alpha=.5)
ax0.semilogx(arange(0, 0.5, 0.5/len(P_n_pink_2)), P_n_pink_2, 'c', alpha=.5)
alpha = 1
ax0.plot([f_min, f_max], [0, 10*log10(pow(f_max, -alpha)) - 10*log10(pow(f_min, -alpha))], 'r--')
ax0.grid(True)
ax0.set_xlim([f_min, f_max])
ax0.set_ylim([P_min, P_max])
ax0.set_xlabel('normalized frequency')
ax0.set_ylabel('normalized power [dB]')
ax0.set_title('Pink noise')
ax0.legend(['PSD', 'AR(' + str(p) + ')', 'theoretical'])
```

```python
# mkdocs: render
# mkdocs: hidecode

import matplotlib.pyplot as plt
from numpy import log10, mean, std, sqrt, arange
from power_law_noise import brownian_noise, pacf, generate_from_AR_model, generate_from_psd
from spectrum import Periodogram

N = 8192
f_min, f_max = 1e-3, 0.5
P_min, P_max = -60, 0
alpha = 2

n_brown_1 = generate_from_psd(N, alpha=2) # brownian_noise(N)
n_brown_2, h_a_1 = generate_from_AR_model(N, alpha=2)
p = len(h_a_1) 
n_brown_2 /= std(n_brown_2)

P_n_brown_1 = 10*log10(Periodogram(n_brown_1).psd/sqrt(N))
P_n_brown_2 = 10*log10(Periodogram(n_brown_2).psd/sqrt(N))
f = arange(0, 0.5, 0.5/len(P_n_brown_1)),

fig1, ax1 = plt.subplots()
ax1.semilogx(arange(0, 0.5, 0.5/len(P_n_brown_1)), P_n_brown_1, 'b', alpha=.5)
ax1.semilogx(arange(0, 0.5, 0.5/len(P_n_brown_2)), P_n_brown_2, 'c', alpha=.5)
alpha = 2
ax1.plot([f_min, f_max], [0, 10*log10(pow(f_max, -alpha)) - 10*log10(pow(f_min, -alpha))], 'r--')
ax1.grid(True)
ax1.set_xlim([f_min, f_max])
ax1.set_ylim([P_min, P_max])
ax1.set_xlabel('normalized frequency')
ax1.set_ylabel('normalized power [dB]')
ax1.set_title('Brownian noise')
ax1.legend(['PSD', 'AR(' + str(p) + ')', 'theoretical'])
```

## Partial autocorrelation
<!-- 
For a power-law noise process, the situation is a bit different compared to a Gaussian process. Power-law noise, characterized by a power-law distribution, often does not have finite moments, including the variance. In such cases, the traditional Gaussian-based approach doesn't directly apply.

However, when dealing with a power-law noise process, it's essential to understand that the moments of the distribution may not exist for certain values of alpha α≤2, the variance is infinite, and higher-order moments may also be undefined.

The lack of finite moments in power-law noise makes the traditional concept of a covariance matrix, which is central to the Gaussian approach, challenging to apply directly. In such cases, alternative methods and statistical descriptors, like the Hurst exponent for long-range dependence or fractal dimensions, are often employed to characterize the behavior of power-law noise processes.

So, in summary, for power-law noise, the direct translation from a covariance matrix and a Gaussian joint PDF as in the Gaussian case may not be straightforward due to the unique characteristics of power-law distributions. Other statistical measures and methods specific to power-law noise analysis are typically used. 
-->


Whitening of a random process is typically being done based on its __autocovariance__, which is a measure of the joint variability of two samples of a process at different points in time. Note that here, and in general in DSP, the terms correlation and covariance are used interchangeably. Strictly speaking, for random processes, correlation refers to standardized covariance, with +1 indicating perfect correlation and −1 indicating perfect anti-correlation. If the sample sequence considered has zero mean and unit variance, then correlation and covariance are indeed the same. Now, the __autocorrelation function (acf)__ for lag $ \tau $ is defined as
$$
r_{xx} (\tau) = \text{E} \left [ x(t + \tau) x(t) \right ] \\ .
$$
For power-law noise, the autocorrelation function extends infinitely because the enduring correlation persists indefinitely over time. In practice, it makes sense to consider the function only up to a finite lag $ \tau $ based on some information criterion, see e.g. the Akaike information criterion ([AIC](https://en.wikipedia.org/wiki/Akaike_information_criterion)), or simply where $ r_{xx} (\tau) $ drops below some threshold value, resulting in a partial autocorrelation function. Using the fundamental Python package NumPy, we can define a simple function to compute a partial autocorrelation function (__pacf__) of a sequence of samples.
```python
def pacf(x: np.array, max_lag: int) -> np.array:
    r = np.correlate(x, x, mode = 'full')[-len(x):]
    return r[:max_lag+1]/r[0]
```

In the figure below, the pacfs of white noise ($ \alpha = 0 $), pink noise ($ \alpha = 1 $), and Brownian noise ($ \alpha = 2 $) realizations are plotted for $ \tau < 10 $.  

```python
# mkdocs: render
# mkdocs: hidecode

import matplotlib.pyplot as plt
from numpy import arange, zeros
from power_law_noise import pacf, generate_from_AR_model, generate_from_psd
from spectrum import Periodogram

alpha_range = [0, 1, 2]
max_pacf_lag = 10  # maximum lag to show for partial autocorrelation function
NFFT = 1024 # 8192

r = zeros((max_pacf_lag + 1, len(alpha_range)))
P = zeros(((NFFT >> 1) + 1, len(alpha_range)))
for i, alpha in enumerate(alpha_range):
    n, h = generate_from_AR_model(NFFT, alpha)
    r[:,i] = pacf(n, max_pacf_lag)
    P_n = Periodogram(n).psd
    P[:,i] = P_n/P_n[0]

k = arange(0, max_pacf_lag) + 1

fig, ax = plt.subplots()
# ax.grid(True)
ax.plot(r[:,0],'blue')
ax.plot(r[:,1],'pink')
ax.plot(r[:,2],'brown')

alpha = 1
model = k**-(1 - alpha/2)
ax.plot(model, 'k--')
# beta = 1 - alpha_range[1]
# model = k**-beta
alpha = 2
model = k**-(1 - alpha/2)
ax.plot(model, 'k--')

ax.set_xlabel('lag')
ax.set_ylabel('autocorrelation function')
ax.legend([f'alpha = {alpha}' for alpha in alpha_range], loc='upper right')
ax.set_xlim([0, max_pacf_lag-1])
ax.set_ylim([-.1, 1.1])    

```

The autocorrelation function of a $ 1 / f $ process can be decribed by 
<!-- [@ward_1f_2007] ?? -->
$$
r_{xx}[k] = k^{-(1 - \alpha/2)} = k^{-0.5} \text{ for } \alpha=1 \\ ,
$$
however, this model does not seem to generalize for arbitray $ \alpha $. In the figure above, this pink noise autocorrelation model is shown as a dotted line. Note that the autocorrelation function can also be obtained from the impulse response of the generating filter convoluted with itself. Since the impulse response consists of a summation of $ p $ exponentially decaying functions, this would probably not lead to a neat closed-form expresion for the autocorrelation function parameterized by $ \alpha $, but we still need to look into that.


## Whitening transformation

For a sequence of samples $ \underline{x} $ of the random sequence $ x[k] $, the __autocorrelation matrix__ $ \bold R $ is defined as
$$
\bold R \triangleq \text{E} \left [ \underline{x} \\, \underline{x}^T \right] \\ , 
$$
where $ \text{E} $ is the expected value operator. By inverting or factorizing the autocorrelation matrix, we can obtain a whitening matrix $ \bold{W} $, i.e.
$$
\bold{R}^{-1} = \bold{W}^T \bold{W} .
$$
Now, a whitened sample sequence $ \underline{y} $ with unit variance is obtained by the transformation
$$
\underline{y} = \bold W \underline{x} .
$$
In practice, the inverted autocorrelation matrix $ \bold{R}^{-1} $ has to be estimated and updated as more samples become available. If we assume a weakly stationary random process, the covariance matrix has Toeplitz structure, which can be approximately diagonalized by the DFT matrix [@gray_asymptotic_1972]. In this case, the inverted covariance matrix can be expressed as 
$$
\bold{R}^{-1} = \bold{F} \text{diag}(\underline{\lambda})^{-1}  \bold{F}^{-1} \\ ,
$$
where $ \bold{F} $ is the DFT matrix, $ \underline{\lambda}$ is the vector of eigenvalues of $ \bold{R} $, and typically $ \bold{W} = \bold{F} \text{diag}(\underline{\lambda})^{-\frac{1}{2}} $. Depending on the size of the autocorrelation matrix this approximation can greatly speed up the process of obtaining and updating a whitening filter. However, in case of power law noise, there is even a simpler method. 

<!-- Note that there are infinitely many possible whitening matrices, for example, Mahalanobis whitening uses $ \bold{W} = \bold{R}^{-\frac{1}{2}}$.  -->


<!--
TODO: blabla
Since the fourier transform of an autocorrelation function, in practice
n estimate of the spectral density of a signal, the periodogram 
 
 the Wiener–Khinchin theorem allows computing the autocorrelation from the raw data X(t) with two fast Fourier transforms (FFT
 A periodogram-based estimate replaces {\displaystyle n-k}n-k in the above formula with {\displaystyle n}n. This estimate is always biased; however, it usually has a smaller mean squared erro
an estimate of the spectral density of a sign -->

### Autoregressive model inversion

Since power law noise can be modelled as an all-pole filtered white noise process, a whitening filter can also be constructed as the __inverted filter__, i.e.
$$
H(z) = {1 + \sum_{k=1}^p \theta_k z^{-k}} \\ .
$$
As an example, Brownian whitening with $ p = 1, \theta_0 = 1, \theta_1 = -1$ is plotted below. 

```python
# mkdocs: render
# mkdocs: hidecode

import matplotlib.pyplot as plt
from numpy import mean, std
from power_law_noise import generate_from_AR_model, generate_from_psd
from scipy.signal import filtfilt

N = 512
alpha = 2

n_brown_1 = generate_from_psd(N, alpha)
n_brown_1 /= std(n_brown_1 - mean(n_brown_1))
filtered = filtfilt([1, -1], 1, n_brown_1)
filtered = filtered/std(filtered)

fig, ax = plt.subplots()
ax.plot(filtered)
ax.plot(n_brown_1, 'brown')
ax.legend(['whitened', 'brown'])
ax.set_xlabel('time')
ax.set_ylabel('normalized amplitude')
ax.set_xticks([])
ax.set_yticks([])
ax.set_title('Brownian noise whitening')
```

To be able to invert the autoregressive model, its coefficients need to be known. One way to estimate the coefficients is through solving the __Yule–Walker equations__, which are given by
$$
\bold R \underline{\theta} = - \underline{r} \\ ,
$$
or
$$
\begin{pmatrix}
   r_0 & r_1 & r_2 & \\cdots & r_{N-1} \\\\
   r_1 & r_0 & r_1 & \\cdots & r_{N-2} \\\\
   \\vdots & \\vdots & \\vdots & \\ddots & \\vdots \\\\
   r_{N-1} & r_{N-2} & r_{N-3} & \\cdot & r_0
\end{pmatrix}
\begin{pmatrix}
\theta_0 \\\\
\theta_1 \\\\
\vdots \\\\
\theta_N
\end{pmatrix} = \begin{pmatrix}
r_0 \\\\
r_1 \\\\
\vdots \\\\
r_N
\end{pmatrix} \\ ,
$$
where $ r_i $ is the i-th autocorrelation coefficient. Again, the inverted autocorrelation matrix $ \bold{R}^{-1} $ has to be estimated in practice and updated as more samples become available, which can become a cumbersome exercise.

### Power law noise identification

If $ \alpha $ is known or can be estimated, an inverted filter is easily constructed from the model coefficients proposed by [@kasdin_discrete_1995], and we do not need to find a solution to the Yule-Walker equations. One approach would be to estimate the psd of the noise process, fit a straight line, and determine the slope. Another approach is to identify the power law noise from the lag 1 autocorrelation, as proposed in [@riley_power_2004], which has substantially lower computational complexity.

If we plot the lag 1 coefficient of power law noise sequences of 1024 samples as a function of $ \alpha $, a sigmoidal relation can be observed on the interval $ 0 \leq \alpha \leq 2 $.

```python
# mkdocs: render
# mkdocs: hidecode

import matplotlib.pyplot as plt
from numpy import arange, zeros
from math import exp
from power_law_noise import pacf, generate_from_AR_model, generate_from_psd
from spectrum import Periodogram

max_pacf_lag = 10  # maximum lag to show for partial autocorrelation function
N = 1024 # 8192
alpha_step = .01
alpha_max = 2.0
alpha_range = arange(alpha_step, alpha_max, alpha_step)
r = zeros((max_pacf_lag + 1, len(alpha_range)))
for i, alpha in enumerate(alpha_range):
    n, h = generate_from_AR_model(N, alpha)
    r[:,i] = pacf(n, max_pacf_lag)

r = [r_[1]/r_[0] for r_ in r.transpose()]
model = [1/(1 + pow(2.718281828, -4 * (alpha-.75))) for alpha in alpha_range]  # sigmoid, for some reason exp cannot be found

fig, ax = plt.subplots()
ax.plot(alpha_range, r) # 10*log10(r))
ax.plot(alpha_range, model, 'k--')
ax.grid(True)
##ax.legend([f'alpha = {alpha}' for alpha in alpha_range])
ax.set_xlabel('alpha')
ax.set_ylabel('lag 1 coefficient')
ax.set_title('Autocorrelation lag 1 of power law processes')
ax.set_xlim([alpha_range[0], alpha_range[-1]])

```

At this moment, I have not yet found an analytical way to compute the relationship, however this should be doable, since the acf equals the convolution of the AR impulse response with its time-reversed version. For now, it's sufficient to note that the first autocorrelation coefficient is approximated by
$$
r_1 \approx \frac {1} {1 + e^{-4 \alpha - 3} } \\ .
$$
The logistic function is linearized as
$$
\ln \left ( 1 / r_1 - 1\right ) \approx -4 \alpha - 3 \\ ,
$$
hence
$$
\alpha \approx \frac{3 - \ln \left ( 1 / r_1 - 1\right )} {4} \\ .
$$
Now that we have an easy way to determine the power law exponent from the first autocorrelation coefficient, let's estimate the coefficient for a sample stream.

### Recursive estimation of autocorrelation coefficients

The $ j $-th partial autocorrelation coefficient of $ x $ is defined as
$$
r_j = \frac {1} {N \sigma^2} \sum_{i=0}^{N} (x[i] - \mu) (x[i + j] - \mu) \\ ,
$$
where $ \mu $ and $ \sigma $ are the mean and standard deviation of the process, respectively. If the process $ x $ is a sample stream, we can define a recursive algorithm that updates the estimate when a new sample becomes available, i.e.
$$
r_1 [k] = \eta \\ r_1 [k - 1] + (1 - \eta) \frac { (x[k] - \mu) (x[k - 1] - \mu)} { \sigma^2} \\ ,
$$
where $ 0 < \eta \leq 1 $ is a forgetting factor, which gives exponentially less weight to older samples. Note that $ \mu  $ and $ \sigma $ can be estimated based on the median and MAD, and $ \sigma \approx k \cdot \text{MAD} $ where $ k \approx 1.4826 $ for Gaussian noise.  Just one memory cell is used to store the previous partial autocorrelation estimate, and another the store the previous sample.

An example implementation of a frugral instantenous lag 1 correlation coefficient estimation with minimal memory usage is given below.

```Python
def estimate_lag_1_correlation(x, eta):
    k = 1.4826
    med, mad, r_ = None, None, None
    r = np.zeros_like(x)
    for i, x_ in enumerate(x):
        alpha = 0.1 if mad is None else mad/100
        med = x_ if med is None else med + alpha if x_ > med else med - alpha
        x_ -= med
        mad = abs(x_) if mad is None else mad + alpha if abs(x_) > mad else mad - alpha
       
        if i > 0:
            if r_ is None:
                r_ = abs(x_ * x_prev_)
            else:
                sigma = k * mad
                r_ = eta * r_ + (1-eta) * x_ * x_prev_ / (sigma**2)
            r[i] = r_
        x_prev_ = x_
    
    return r
```

In the figure below, examples are shown.
For $ \alpha = 0.5 $, identification of the power law process seems reasonable, but for higher values of 
$ \alpha $, the algorithm perfroms very poor. It seems that the long memory-effect of power law noise does not allow a frugral algorithm to estimate the statistics correctly. 

```python
# mkdocs: render
# mkdocs: hidecode

import matplotlib.pyplot as plt
from numpy import zeros, zeros_like, log
from math import exp
from power_law_noise import pacf, generate_from_AR_model, generate_from_psd
from estimate_lag_1_correlation import estimate_lag_1_correlation

max_pacf_lag = 10  # maximum lag to show for partial autocorrelation function
N = 8192
alpha_step = .5
alpha_max = 2.0
alpha_range = arange(alpha_step, alpha_max, alpha_step)
r = zeros((max_pacf_lag + 1, len(alpha_range)))
r_est = zeros((N, len(alpha_range)))

for i, alpha in enumerate(alpha_range):
    n, h = generate_from_AR_model(N, alpha)
    r[:,i] = pacf(n, max_pacf_lag)
    r_est[:,i] = estimate_lag_1_correlation(n, .999)


fig, ax = plt.subplots()
ax.grid(True)
ax.set_title('power law exponent estimate')
ax.plot((3 - log(1/(r_est+1e-3) - 1)) / 4)
ax.plot([0, N-1], 2 * [alpha_range[0],], 'b--')
ax.plot([0, N-1], 2 * [alpha_range[1],], 'y--')
ax.plot([0, N-1], 2 * [alpha_range[2],], 'g--')
ax.set_xlabel('sample [#]')
ax.set_ylabel('estimated alpha')
ax.legend([f'alpha = {alpha}' for alpha in alpha_range], loc='upper right')
ax.set_xlim([0, N-1])
ax.set_ylim([0, 2])
```

## Conclusion

* In practice, noise sequences often have correlated samples, giving rise to non-flat power spectra. One common example of a colored noise process is power law noise, or $ 1/ f^\alpha $ noise.

* Many DSP methods assume to deal with white noise processes, since processing uncorrelated noise samples is typically more straightforward compared to colored noise samples. One approach to deal with colored noise is to apply a whitening filter prior to further processing, such as outlier detection.

* In general, colored noise can be approximately whitened through factorizing the autocorrelation matrix. Power law noise processes seem to exhibit infinitely long range dependence and as such only partial correlation functions can be estimated. Another approach is to whiten the noise process in the frequency domain.

* Power law noise can be generated by either filtering white noise as to approximate the desired power spectral density of the colored noise spectrum, or by an autoregressive model (all-pole filter) to correlate white noise. For that reason, a whitening filter for power law noise can be constructed as the inverted autoregressive model.

* Power law noise can be identified, or in other words $ \alpha $ can be estimated, either by evaluating the power spectral density, or by interpreting the lag 1 autocorrelation coefficient. The latter of which is easier to compute. 

* An attempt was made to design a frugral algorithm with extremely low memory usage and computatonal complexity as to estimate the lag 1 autocorrelation coefficient. This approach seems to result in biased estimates of $ \alpha $ for reasons currently unknown.


<!-- How to choose p?, how to choose N , so define partial autocorrelation function
the ACF is exponentially decaying o

 

Practical implementation of a Kalman Filter is often difficult due to the difficulty of getting a good estimate of the noise covariance matrices Qk and Rk. Extensive research has been done to estimate these covariances from data. One practical method of doing this is the autocovariance least-squares (ALS) technique that uses the time-lagged autocovariances of routine operating data to estimate the covariances.[27][28] The GNU Octave and Matlab code used to calculate the noise covariance matrices using the ALS technique is available online using the GNU General Public License.[29] Field Kalman Filter (FKF), a Bayesian algorithm, which allows simultaneous estimation of the state, parameters and noise covariance has been proposed.[30] The FKF algorithm has a recursive formulation, good observed convergence, and relatively low complexity, thus suggesting that the FKF algorithm may possibly be a worthwhile alternative to the Autocovariance Least-Squares methods.

In density based methods, outliers and regular samples are distinguished based on their instantenous values alone.

The statistics based approaches presume that signal samples are uncorrelated, implying that that the autocorrelation function of the signals equals \delta



another approach is dynamically adapt thresholds based on covar matrix, we don't need any inversion now


Recursive estimation of the Median Covariation Matrix in
Hilbert spaces


Test with noise generated by FFM according to https://www.researchgate.net/publication/360161513_On_the_Autocorrelation_Function_of_1f_Noises page 2,3  


Online estimation of the inverse of the covariance matrix, see e.g Moon 259, but also https://en.wikipedia.org/wiki/Recursive_least_squares_filter


Or should we use online psdf estimation, periodogram

https://en.wikipedia.org/wiki/Whitening_transformation

https://multimed.org/denoise/whiten.html


Some estimators are highly sensitive to outliers, notably estimation of covariance matrices.

https://en.wikipedia.org/wiki/Estimation_of_covariance_matrices
covariance (ass. multi-dim Gaussian on elliptic envelope, boundary from mahdonobis distance)


https://github.com/dganguli/robust-pca -->



\bibliography